# Timestamped Serial Logging

This program helps you to log textual (best suited for CSV) data from a serial port.

It ships both a Python variant (`python -m logserial` or just `log-serial`) and a POSIX shell variant (`log-serial.sh`).

## ✨ Features

The `log-serial` (or `python -m logserial`) Python executable and the shell script `log-serial.sh` both do essentially the same:

- write an optional header to the file (`-H 'field1,field2,field3'`)
- read lines from a serial port (e.g. `/dev/ttyUSB0`, auto-detected if only one connected, specify `-p /dev/ttyBLA` otherwise)
- prepend a timestamp to each line (e.g. `2022-11-08T16:15:35.497478+0100`, specify `-t '%Y-%m-%d %H:%M:%S'` for custom [strftime format](https://strftime.org/))
- show output in the terminal (e.g. to pipe into [`polt`](https://gitlab.com/nobodyinperson/python3-polt) for live plotting, e.g. `log-serial -n | polt add-source -p csv live`)
- write all of it into a file (e.g. `2022-11-08T15:57:26.137182934+0100-log-serial.csv`, specify `-f '%Y-%m-%d-%H-%M-%S-myfile.csv'` for custom naming)

Furthermore the shell script `log-serial.sh`:

- avoids common pitfalls when logging from serial (e.g. buffering, forgetting to save, forgetting to specify millisecond time precision, etc...)
- is POSIX-compliant (i.e. should run unter bash, zsh, dash, etc.)
- only really needs `coreutils`, optionally `sed` and `moreutils` (for `ts`).
- still works without some of its dependencies (e.g. `tee` and `ts` which are sometimes not available, e.g. under [Git Bash](https://gitforwindows.org/))

The Python version `log-serial` or `python -m logserial` can be used if the shell script doesn't work or vice-versa.

## 📥 Installation

```bash
# Install as a normal Python package
pip install git+https://gitlab.com/tue-umphy/software/log-serial
```

<details>
<summary>🖱️ Click here if the above doesn't work</summary>

```bash
# update pip
python -m pip install --user -U pip  # (also try without the `--user` and `--U`)
# then install
python -m pip install --user git+https://gitlab.com/tue-umphy/software/log-serial
```

</details>

<details>
<summary>🖱️ Click here for shell script-only installation instructions</summary>

## 🐚 POSIX-Script-Only Installation

```bash
# Download the log-serial script into the hidden folder .local/bin in your home directory:
wget -P "$HOME/.local/bin" https://gitlab.com/tue-umphy/software/log-serial/-/raw/main/log-serial.sh
# alternatively, if wget is not installed, try curl:
curl -o "$HOME/.local/bin/log-serial" https://gitlab.com/tue-umphy/software/log-serial/-/raw/main/log-serial.sh

# Make the script executable
chmod +x "$HOME/.local/bin/log-serial.sh"

# Now (eventually after reopening your terminal) you should be able to run the script from anywhere:
log-serial.sh -h  #  👈  the -h display help page
```

<details>
<summary>🖱️ Click here if the above instructions don't work</summary>

### ❓ Installation Troubleshooting:

```bash
# If the above doesn't work, your ~/.local/bin folder is probably not in your PATH, check with
echo $PATH  # 👈  if you don't see something like /home/YOU/.local/bin, then that's the problem

# To fix, first find out your shell:
echo $SHELL

# Then permanently update your shell's PATH (depends on your shell)
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc            # 👈  for bash shell
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.zshrc             # 👈  for zsh shell
echo 'fish_add_path $HOME/.local/bin' >> ~/.config/fish/config.fish # 👈  for fish shell

# Then after reopening your terminal, log-serial should work:
log-serial.sh -h  #  👈  the -h display help page

# If that STILL doesn't work, then just download log-serial into your current directory and run it from there:
wget https://gitlab.com/tue-umphy/software/log-serial/-/raw/main/log-serial
# Make it executable
chmod +x log-serial.sh
# Then run it
./log-serial.sh -h  #  👈  the -h display help page
# 👆 Note the ./ (dot slash) in front
# If that doesn't work, try:
sh log-serial.sh -h  # 👈  you can also try 'bash' or 'zsh' instead of 'sh' in front if it doesn't work
```

If that still doesn't work, you probably just have a crooked system... 🫤

</details>

</details>


## ❓ Usage

See `log-serial -h` (or `log-serial.sh -h` or `python -m logserial -h`) for help.

Example usage:

```bash
# Logs from the only connected serial port into a timestamped CSV file at 9600 baud
log-serial

# More options
log-serial -p /dev/ttyACM0 -b 115200 -H 'time,temperature,humidity' -f '%FT%T-myfile.csv'
```

This creates a file named like `2022-11-08T17:02:43.752055526+0100-log-serial.csv` with the following content:

```csv
#   command-line: log-serial -b 115200 -H time,temperature,humidity
#   reproducible command-line: log-serial -p '/dev/ttyUSB0' -b '115200' -t '%Y-%m-%dT%H:%M:%.S%z,' -f '%Y-%m-%dT%H:%M:%S.%N%z-log-serial.csv' -s '0'
time,temperature,humidity
2022-11-08T17:02:44.928101+0100, 21.1,41.2
2022-11-08T17:02:44.971130+0100, 21.2,42.8
2022-11-08T17:02:49.081306+0100, 21.2,42.3
2022-11-08T17:02:49.081536+0100, 21.3,40.2
2022-11-08T17:02:49.132047+0100, 21.2,41.4
2022-11-08T17:02:49.135700+0100, 21.4,46.2
2022-11-08T17:02:49.154255+0100, 21.5,39.2
2022-11-08T17:02:49.208632+0100, 21.5,41.8
2022-11-08T17:02:49.611104+0100, 22.2,42.1
...
```

### 💥 Troubleshooting:

- if your Windows doesn't find the shell script `log-serial.sh`, try `pip show -f log-serial` to show the installation path and specify the full path in the terminal instead
- otherwise just download the script into your working directory as described above
- serial ports in Git Bash on Windows sometimes show up as something like `/dev/ttyS4` (instead of `/dev/ttyACM0`), just try it with `log-serial -p /dev/ttyS3`.
- If [`polt`](https://gitlab.com/nobodyinperson/python3-polt) under Windows gives weird encoding errors, try not piping the output of `log-serial` into it but specify it as a parser command:

  ```bash
  # 🚫 Don't pipe it into polt:
  python -m logserial -b 115200 -p /dev/ttyS3 -H time,col1,col2,col3 --no-comments | polt add-source -p csv live
  # 👉 instead, specify as command
  python -m polt add-source -c 'python -m logserial -b 115200 -p /dev/ttyS3 -H time,col1,col2,col3 --no-comments' -p csv live
  # 👉 or you can try a different Matplotlib backend
  python -m logserial -b 115200 -p /dev/ttyS3 -H time,col1,col2,col3 --no-comments | MPLBACKEND=TkAgg polt add-source -p csv live
  # If TkAgg is not available, Matplotlib will complain and list available backends
  ```

