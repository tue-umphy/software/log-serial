#!/usr/bin/env python3
# system modules
import argparse
import datetime
import socket
import logging
import os
import shlex
import sys
import re

# external modules
import serial
from serial.tools.list_ports import comports

# internal modules
from logserial.version import __version__


def now():
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)


parser = argparse.ArgumentParser(
    prog="python -m logserial" if "__main__" in sys.argv[0] else sys.argv[0],
    description=r"""
 _                                        _         _
| |  ___    __ _         ___   ___  _ __ (_)  __ _ | |
| | / _ \  / _` | _____ / __| / _ \| '__|| | / _` || |
| || (_) || (_| ||_____|\__ \|  __/| |   | || (_| || |
|_| \___/  \__, |       |___/ \___||_|   |_| \__,_||_|
           |___/
"""
    f"""
Log and timestamp data from a serial port

ℹ️   Hint: Try the 'log-serial.sh' program shipped with this package if this doesn't work.

🏷️  version {__version__}
""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
parser.add_argument("-p", "--port", help="Serial port to log from")
parser.add_argument(
    "-b",
    "--baudrate",
    help="Baudrate to connect to serial port",
    type=int,
    default=9600,
)
parser.add_argument(
    "-H", "--header", help="Header to prepend to the output file"
)
parser.add_argument(
    "-t",
    "--timeformat",
    help="Format of the timestamp in the output file",
    default="%Y-%m-%dT%H:%M:%S.%f%z",
)
parser.add_argument(
    "-f",
    "--filenameformat",
    help="Template for the output filename"
    + (
        "These placeholders are available: "
        "{id}: (from -i/--id argument), "
        "{port}: sanitized port "
    ),
    default="%Y-%m-%dT%H.%M.%S.%f%z-{id}-{port}.csv",
)
parser.add_argument(
    "-i",
    "--id",
    "--identifier",
    help="identifier for use as the {{id}} placeholder in --filenameformat. Defaults to {!r}".format(
        default := "log-serial"
    ),
    default=default,
)
parser.add_argument(
    "-s",
    "--skiplines",
    help="Skip this many lines from the serial port before writing to the output file",
    type=int,
    default=0,
)
parser.add_argument(
    "--timeout",
    help="Timeout for the serial connection",
    type=float,
    default=5,
)
parser.add_argument(
    "-n",
    "--no-comments",
    action="store_true",
    help="don't add comments to file",
)
parser.add_argument(
    "-v", "--verbose", action="store_true", help="verbose output"
)
parser.add_argument(
    "-q", "--quiet", action="store_true", help="only show errors"
)
parser.add_argument(
    "--version", action="store_true", help="show version and exit"
)


def cli():
    logger = logging.getLogger(parser.prog)

    args = parser.parse_args()

    if args.version:
        print(__version__)
        sys.exit(0)

    logging.basicConfig(
        level=logging.DEBUG
        if args.verbose
        else (logging.ERROR if args.quiet else logging.INFO)
    )

    if not (port := args.port):
        if len(serial_ports := comports()) == 1:
            port = serial_ports[0].device
            logger.info(f"ℹ️  Auto-selected the only serial port {port}")
        elif not serial_ports:
            parser.error("🤷 No serial ports found on the system.")
        elif len(serial_ports) > 1:
            parser.error(
                f"{len(serial_ports)} serial ports on the system "
                f"({', '.join([repr(p.device) for p in serial_ports])}). "
                "Select one with `-p PORT`"
            )

    logger.info(f"Opening serial port {port} @ {args.baudrate}bps")
    device = serial.Serial(
        port=port,
        baudrate=args.baudrate,
        timeout=args.timeout,
    )

    # skip lines
    for i, line in zip(range(args.skiplines), device):
        logger.debug(f"🗑️ skipping line {i + 1}: {line = }")

    filepath = now().strftime(
        args.filenameformat.format(
            id=args.id, port=re.sub(r"[^0-9a-zA-Z_.-]+", r"-", port)
        )
    )
    with open(filepath, "w") as outfile:
        # comments
        if not args.no_comments:
            for fh in (outfile, sys.stdout):
                fh.write(
                    f"#                        file: "
                    f"{os.path.basename(filepath)}\r\n"
                )
                fh.write(
                    f"#         actual command-line: "
                    f"{os.getlogin()}@{socket.gethostname()}> "
                    f"{' '.join(map(shlex.quote,[os.path.basename(sys.argv[0])]+sys.argv[1:]))}\r\n"
                )
                reprocmd = {
                    "-p": port,
                    "-b": args.baudrate,
                    "-t": args.timeformat,
                    "-f": args.filenameformat,
                    "-i": args.id,
                    "-s": args.skiplines,
                }
                fh.write(
                    f"#   reproducible command-line: "
                    f"{os.getlogin()}@{socket.gethostname()}> "
                    f"python -m logserial {' '.join(k+' '+shlex.quote(str(a)) for k,a in reprocmd.items() if a)}\r\n"
                )
                fh.flush()

        # header
        if args.header:
            for fh in (outfile, sys.stdout):
                fh.write(f"{args.header}\r\n")
                fh.flush()

        # data
        for line in device:
            line = line.decode(errors="ignore").strip()
            outline = f"{now().strftime(args.timeformat)},{line}\r\n"
            for fh in (outfile, sys.stdout):
                fh.write(outline)
                fh.flush()


if __name__ == "__main__":
    cli()
