#!/bin/sh

debug() (
	echo >&2 "ℹ️  [$(basename "$0")] $*"
)

strip_whitespace() (
    if command -v sed 2>/dev/null >/dev/null;then
        sed 's|^\s*||g;s|\s*$||g'
    elif command -v perl 2>/dev/null >/dev/null;then
        perl -pe 's|^\s*||g;s|\s*$||g'
    else
        cat
    fi
)

find_ports() (
    (
        # find ports with Python
        python -m serial.tools.list_ports 2>/dev/null
        # also find ports with globs
        shopt -s nullglob 2>/dev/null || true
        setopt CSH_NULL_GLOB 2>/dev/null || true
        # TODO: Add globs for MacOS!
        for port in /dev/ttyS* /dev/ttyACM* /dev/ttyAMA* /dev/ttyUSB*; do
            if test -e "$port"; then
                echo "$port"
            fi
        done
    ) | strip_whitespace | sort -u
)

usage() (
	cat >&2 <<'EOT'
 _                                        _         _          _     
| |  ___    __ _         ___   ___  _ __ (_)  __ _ | |    ___ | |__  
| | / _ \  / _` | _____ / __| / _ \| '__|| | / _` || |   / __|| '_ \ 
| || (_) || (_| ||_____|\__ \|  __/| |   | || (_| || | _ \__ \| | | |
|_| \___/  \__, |       |___/ \___||_|   |_| \__,_||_|(_)|___/|_| |_|
           |___/                                                     
EOT
	cat >&2 <<EOT

This is the shell-variant installed alongside the Python logserial package:

    https://gitlab.com/tue-umphy/software/log-serial

You can try this if 'log-serial' or 'python -m log-serial' doesn't work.

Usage: 
    
    $(basename "$0") [-p PORT] [-b BAUDRATE] [-H HEADER] [-t TIMEFORMAT] [-f FILENAME_DATEFORMAT] [-s SKIP_N_LINES]"

Example:
    
    To log from the only serial port connected, just run:

        $(basename "$0")

    To log from an Arduino connected as ttyUSB0 with 115200 baurate which outputs
    two data columns separated with a comma, run:

        $(basename "$0") -p /dev/ttyUSB0 -b 115200 -H time,temperature,humidity -f '%FT%T-arduino.csv'

EOT
)

# defaults
BAUDRATE=9600
TIMEFMT='%Y-%m-%dT%H:%M:%.S%z,'
FILENAMEFMT="%Y-%m-%dT%H:%M:%S.%N%z-$(basename "$0").csv"
SKIPLINES=0
COMMENTS=1

# read arguments
while getopts ":p:H:t:b:f:s:n" arg; do
	case "$arg" in
	p) PORT="$OPTARG" ;;
	H) HEADER="$OPTARG" ;;
	t) TIMEFMT="$OPTARG" ;;
	b) BAUDRATE="$OPTARG" ;;
	f) FILENAMEFMT="$OPTARG" ;;
	s) SKIPLINES="$OPTARG" ;;
	n) COMMENTS= ;;
	*)
        echo "💥  Invaild usage! 💥"
        echo
		usage
		exit 1
		;;
	esac
done

if test -z "$PORT"; then
	ports="$(find_ports)"
    nports="$(echo "$ports" | grep -cv '^\s*$')"
	if test "$nports" -eq 1; then
		PORT="$(echo "$ports" | head -n1)"
		debug "Auto-selected port $PORT"
	else
        debug "$nports ports found.  $(echo $ports)"
		debug "Please specify serial port with e.g.    $(basename "$0") -p $(echo "$ports" | head -n1)"
		exit 1
	fi
fi

ts_ify_timefmt() (
	if command -v sed 2>/dev/null >/dev/null; then
		echo "$*" | sed 's|%S\.%N|%.S|g ; s|%s\.%N|%.s|g'
	else
		echo "$*"
	fi
)

date_ify_timefmt() (
	if command -v sed 2>/dev/null >/dev/null; then
		echo "$*" | sed 's|%\.S|%S.%N|g ; s|%\.s|%s.%N|g'
	else
		echo "$*"
	fi
)

prepend_timestamp() (
	if test -n "$TIMEFMT"; then
		if command -v ts 2>/dev/null >/dev/null; then
			# the 'ts' command doesn't know %N, but %.S and %.s
			TIMEFMT="$(ts_ify_timefmt "$TIMEFMT")"
			ts "$TIMEFMT"
		else
			debug "command 'ts' not available. Using while loop with 'date' command."
			# the 'date' command doesn't know %.S and %.s, but %S and %N
			TIMEFMT="$(date_ify_timefmt "$TIMEFMT")"
			while IFS="$(printf '\n')" read -r line; do
				echo "$(date +"$TIMEFMT")$line"
			done
		fi
	else
		cat
	fi
)

write_to_file_and_stdout() (
	if command -v tee 2>/dev/null >/dev/null; then
		tee "$1"
	else
		debug "command 'tee' not available. Using while loop."
		while IFS="$(printf '\n')" read -r line; do
			echo "$line"
			echo "$line" >>"$1"
		done
	fi
)

# log from serial port
filepath="$(date +"$FILENAMEFMT")"
(
	if test -n "$COMMENTS";then
		echo "#  $(basename "$filepath")"
		echo "#         actual command-line: $USER@$(hostname)> $(basename "$0") $*"
		echo "#   reproducible command-line: $USER@$(hostname)> $(basename "$0") -p '$PORT' -b '$BAUDRATE' -t '$TIMEFMT' -f '$FILENAMEFMT' -s '$SKIPLINES' $(test -n "$COMMENTS" && echo -- '-n')"
	fi
	test -z "$HEADER" || echo "$HEADER"
	(
		stty raw "$BAUDRATE"
		cat
	) <"$PORT" | (
		if test "$SKIPLINES" -gt 0; then
			debug "Skipping $SKIPLINES lines from $PORT"
			stdbuf -oL tail -n+"$((1 + "$SKIPLINES"))"
		else cat; fi
	) | prepend_timestamp
) | (
	debug "Logging output of $PORT@$BAUDRATE to '$filepath'"
	write_to_file_and_stdout "$filepath"
)
